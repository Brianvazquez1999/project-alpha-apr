TASK TRACKER:
Hi, welcome to my TASK TRACKER application. I used Django and python for the backend and HTML and CSS for the frontend.

Description:
This application allows users to add projects that they are working on and add tasks that they need to complete. Allowing users to effortlessly track their progress for said project. Users can add a description for each project. Additionally, users can assign a name to each task, designate a start and due date, and checkoff if it is completed. Users can also view a list of their projects and a list of their tasks.

Instructions:
1. Fork this repository.
2. Clone your forked repository.
3. CD into forked repository and type python manage.py runserver.
