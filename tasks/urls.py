from django.urls import path
from .views import create_task, show_my_tasks, update_task, task_detail

urlpatterns = [
    path("<int:id>/detail", task_detail, name="task_detail"),
    path("<int:id>/update", update_task, name="update_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/create/", create_task, name="create_task"),
]
