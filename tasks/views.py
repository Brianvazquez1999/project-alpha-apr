from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task
from projects.models import Project
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def create_task(request, id):
    post = Project.objects.get(id=id)

    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():

            task = form.save(False)
            print(post, post.owner)
            task.project = post
            task.assignee = post.owner
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": my_tasks}
    return render(request, "tasks/list.html", context)

def task_detail(request, id):
    detail = Task.objects.get(id=id)
    context = {
        "detail": detail
    }
    return render(request, "tasks/detail.html", context)


def update_task(request, id):
    post = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=post)
        if form.is_valid():
            post =form.save()
            return redirect("show_project", id=post.project.id)
    else:
        form = TaskForm(instance=post)
    context = {
        "form":form
    }
    return render(request, "tasks/update.html", context)
