from django.urls import path
from .views import list_projects, show_project, create_project, delete_project

urlpatterns = [
    path("<int:id>/delete/", delete_project, name="delete_project"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    path("", list_projects, name="list_projects"),
]
